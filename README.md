# Objetivo
Projeto que visa oferecer um modelo para entendimento de arquitetura de microserviços proposta baseada no modelo hexagonal clássico.

# Organização
Os projetos são organizados obedecendo a estrutura apresentada abaixo:
> ### ciee-service-parent
> ### ciee-service-api
> ### ciee-service-api-client
> ### ciee-service-core
> ### ciee-service-build

Conforme apresentado, o projeto é organizado por módulos visando sua organização e fácil entendimento
Os projetos tem suas funções conforme descrito a seguir:

* ciee-service-parent:
Tem como objetivo agrupar os módulos e controlar as dependências utilizadas pelos projetos, bem como suas versões

* ciee-service-api-client:
Define as interfaces para exposição da API REST. Todo endpoint de comunicação deverá ter uma interface anotada com métodos de API e seus
respectivos parâmetros. Este módulo tem como finalidade ser oferecido como biblioteca para todos os demais serviços que comuniquem com
esta API, com a finalidade de manter o contrato, documentar o contrato e controlar seu versionamento.

* ciee-service-api:
A implementação dos contratos definidos no módulo acima. Aqui contém toda a camada de conversão de entidades em objetos de transporte,
chamada da camada core, inclusão de camada de serviços se necessário e chamada de outras APIs.

* ciee-service-core:
Toda a regra de negócio deverá estar implementada aqui. Mapeamento de entidades, acesso a dados, etc. Tudo o que for pertinente ao negócio
deve conter neste módulo e também classes utilitárias que sejam utilizadas em mais de um dos módulos da aplicação.

* ciee-service-build:
O agregador de funcionalidades que será responsável por gerar o executável do microserviço. Aqui deverá ser incluída a classe de subida do
Spring Boot, o arquivo bootstrap.yml e demais configurações referentes a frameworks.

# Utilização
O projeto apesar de simples e se utilizar de exemplos que não condizem com a realidade do projeto, pode servir como base para entendimento
do modelo de implementação proposto, uma vez que as classes estão devidamente separadas de acordo com seu contexto. Ao entrar na raiz do 
projeto podemos executar o comando **mvn archetype:generate** e seguir o wizard para criar um novo microserviço já com as características do projeto modelo.