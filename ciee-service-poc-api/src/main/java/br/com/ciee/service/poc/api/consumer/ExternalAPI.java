/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.service.poc.api.consumer;

import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * Interface que define a comunicação com um endpoint externo. Esta inferface
 * deve estender uma classe declarada no módulo api-client de outra api.
 * @author vitor
 */
@FeignClient("other-service")
public interface ExternalAPI extends ExampleRestClient {
    
}
