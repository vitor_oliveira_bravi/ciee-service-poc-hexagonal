/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.service.poc.api;

import br.com.ciee.service.poc.business.entity.Person;
import br.com.ciee.service.poc.business.repository.PersonRepository;
import br.com.ciee.service.poc.client.HelloService;
import br.com.ciee.service.poc.client.dto.HelloDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vitor
 */
@RestController
public class HelloWorld implements HelloService {
    
    @Autowired
    private PersonRepository repository;
    
    @Value("${hello.message}")
    private String message;

    @Override
    public HelloDTO hello(String name) {
        Person person = repository.get(name);
        
        HelloDTO dto = new HelloDTO();
        dto.setName(person.getName());
        dto.setMessage(message);
        return dto;
    }
}