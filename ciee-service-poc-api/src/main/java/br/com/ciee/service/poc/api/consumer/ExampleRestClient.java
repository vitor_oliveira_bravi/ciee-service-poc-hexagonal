/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.service.poc.api.consumer;

import br.com.ciee.service.poc.client.dto.HelloDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Interface para emular a declaração de api externa
 * @author vitor
 */
public interface ExampleRestClient {
    /*
     * Método de exemplo para simular a exposição de uma API Rest
     */
    @GetMapping("/hello/message")
    HelloDTO getHelloMessage(@RequestParam("personName") String name);

}
