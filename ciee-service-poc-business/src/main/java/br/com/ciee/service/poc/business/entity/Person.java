/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.service.poc.business.entity;

import lombok.Data;

/**
 *
 * @author vitor
 */
@Data
public class Person {
    
    private int id;
    private String name;
    
}
