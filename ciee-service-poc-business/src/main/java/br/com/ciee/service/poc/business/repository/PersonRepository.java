/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.service.poc.business.repository;

import br.com.ciee.service.poc.business.entity.Person;
import org.springframework.stereotype.Component;

/**
 *
 * @author vitor
 */
@Component
public class PersonRepository {
    
    public Person get(String name) {
        Person person = new Person();
        person.setId(Integer.MIN_VALUE);
        person.setName(name);
        return person;
    }
}
