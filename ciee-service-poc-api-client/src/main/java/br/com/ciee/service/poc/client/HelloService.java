/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.service.poc.client;

import br.com.ciee.service.poc.client.dto.HelloDTO;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Contrato do serviço de API.
 * @author vitor
 */
public interface HelloService {
    /**
     * Método que retorna um oi para o consumidor
     * @param name O nome da pessoa.
     * @return O objeto com os valores de retorno.
     */
    @GetMapping("/hello")
    @Secured("ADMIN")
    HelloDTO hello(@RequestParam("name") String name);
}