/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.service.poc.client.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Classe que define os retornos para 
 * @author vitor
 */
@Data
@NoArgsConstructor
public class HelloDTO {
    
    private String name;
    private String message;
}